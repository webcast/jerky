If you are deploying to kubernetes, you have to create configmaps and secrets.
config.py:

kubectl create configmap app-config --from-file=config.py

uwsgi.ini

kubectl create configmap wsgi-config --from-file=uwsgi.ini

create secret for the handbrake private key

kubectl create secret generic handbrake-key  --from-file=handbrake_rsa

create a tls keys for ingress

openssl req -newkey rsa:2048 -x509 -sha256 -days 365 -nodes -out tls.crt -keyout tls.key -subj "/CN=jerky.cern.ch/emailAddress=domarack@cern.ch"

kubectl create secret tls jerky-tls-cert --key=tls.key --cert=tls.crt
