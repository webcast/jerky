from app.secret import config


class OauthConfig(object):
    """
    CERN Oauth configuration
    """
    CERN_OAUTH_CLIENT_ID = config.CERN_OAUTH_CLIENT_ID
    CERN_OAUTH_CLIENT_SECRET = config.CERN_OAUTH_CLIENT_SECRET
    CERN_OAUTH_AUTHORIZE_URL = config.CERN_OAUTH_AUTHORIZE_URL
    CERN_OAUTH_TOKEN_URL = config.CERN_OAUTH_TOKEN_URL



class DatabaseConfig(object):
    """
    Application database configuration
    """
#     DB_NAME = config.DB_NAME
#     DB_PASS = config.DB_PASS
#     DB_PORT = config.DB_PORT
#     DB_SERVICE = config.DB_SERVICE
#     DB_USER = config.DB_USER

    SQLALCHEMY_TRACK_MODIFICATIONS = False
#     SQLALCHEMY_RECORD_QUERIES = False
#     SQLALCHEMY_POOL_SIZE = config.SQLALCHEMY_POOL_SIZE
#     SQLALCHEMY_POOL_TIMEOUT = config.SQLALCHEMY_POOL_TIMEOUT
#     SQLALCHEMY_POOL_RECYCLE = config.SQLALCHEMY_POOL_RECYCLE
#     SQLALCHEMY_MAX_OVERFLOW = config.SQLALCHEMY_MAX_OVERFLOW

    """
    Database URI will be generated with the already gotten database parameters
    """
    SQLALCHEMY_DATABASE_URI =  'sqlite:////opt/app-root/transcode.db'

class BaseConfig(OauthConfig, DatabaseConfig):
    """
    Stores the app configuration
    """

    ADMIN_EGROUP = config.ADMIN_EGROUP
    APP_PORT = config.APP_PORT
    SECRET_KEY = config.SECRET_KEY

    """
    Debug and logging configuration
    """
    DEBUG = config.DEBUG
    IS_LOCAL_INSTALLATION = config.IS_LOCAL_INSTALLATION

    LOG_LEVEL = config.LOG_LEVEL
    WEBAPP_LOGS = config.WEBAPP_LOGS

    """
    Whether or not to use the wsgi Proxy Fix
    """
    USE_PROXY = config.USE_PROXY
    try:
        SERVER_NAME = config.SERVER_NAME
    except AttributeError:
        pass
