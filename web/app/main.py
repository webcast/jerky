#!flask/bin/python
from app.app_factory import create_app
from app.settings import BaseConfig
from app.common.logger import setup_webapp_logs
from app.secret import config

from app.cli import initialize_cli


app = create_app(BaseConfig)
setup_webapp_logs(app, to_file=False, webapp_log_path="")

initialize_cli(app)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=app.config['APP_PORT'], debug=app.config['DEBUG'], ssl_context=config.SSL)





# @app.route('/', methods=['GET', 'POST'])
# def index():
#     originpath = MasterPathForm(request.form)
#     if request.method == 'POST':
#         return transcode_results(originpath)

#     return render_template('index.html', form=originpath)


# @app.route('/results')
# def transcode_results(form):
#     app.logger.debug(form.data['originpath'])
#     path = os.path.normpath(form.data['originpath'])
#     app.logger.debug(path)
#     cmd = utils.ffmpeg_cmd(path)
#     utils.ffmpeg_run(cmd)

#     #  if not results:
#     flash('No results found!')
#     return redirect('/')
#     #else:
#         # display results
#     #    return render_template('results.html', results=results)

