from app.extensions import db


class Transcode(db.Model):
    __tablename__ = "transcode"
    id = db.Column(db.Integer, primary_key=True)
    file = db.Column(db.String)

    def __repr__(self):
        return "<Origin Path: {}>".format(self.file)
