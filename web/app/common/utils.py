# coding=utf8

import glob
import os
import random
import sys
import re
import paramiko

from flask import current_app, url_for

def str2bool(v):
    """
    Converts a string to boolean

    :param v: Value to convert to boolean
    :return: The boolean result
    """
    return v.lower() in ("yes", "true", "t", "1")

def init_ssh_client():
    """
    Initializes and ssh connection to a remote machine
    :return: the ssh client
    """
    try:
        ssh = paramiko.SSHClient()
        # automatically adds host to known hosts list
        k = paramiko.RSAKey.from_private_key_file('handbrake_rsa')
        #k = paramiko.RSAKey.from_private_key_file(os.environ['SSH_KEY'])
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        #current_app.logger.debug("CONNECTING at {}@{}".format(os.environ['SSH_USERNAME'], os.environ['SSH_SERVER']))
        #ssh.connect(os.environ['SSH_SERVER'], username=os.environ['SSH_USERNAME'], pkey=k)
        current_app.logger.debug("CONNECTING at {}@{}".format('root', 'handbrake01.cern.ch'))
        ssh.connect('handbrake01.cern.ch', username='root', pkey=k)
        current_app.logger.debug('Connected to the SSH client')
    except Exception as e:
        current_app.logger.error("failled to init ssh client: {}".format(e))
    return ssh

def ffmpeg_cmd(originpath):
    """
    prepare the ffmpeg command from the given originpath
    :param originpath: can use only originpath on DFS including the file  name
    :return: full command line which will be exectuted on the remote machine.
    """
    mount_point= '/mnt/master/Rooms'
    root_origin_path = '\\cern.ch\\dfs\\Services\\Recordings\\Rooms\\'
    ffmpeg_transcode = "./ffmpeg -i {} -y -c:a aac -strict -2 -b:a 96k -c:v libx264 -s 1920x1080 -crf 21 -preset medium {} -nostdin -nostats > output.log 2>&1 < /dev/null &"

    #searching for camera or slides.mp4
    regex = r".{6}\.mp4"
    try:
        relative_path = (originpath.split(root_origin_path)[1]).replace('\\','/')
        current_app.logger.debug("relative_path file path: {}".format(relative_path))
        input_file_path = os.path.join(mount_point,relative_path)
        current_app.logger.debug("input file path: {}".format(input_file_path))
        sub_path  = re.sub(regex,'',relative_path)
        current_app.logger.debug("sub_path file path: {}".format(sub_path))
        output_file_path = os.path.join(mount_point,sub_path,"transcoded_ffmpeg.mp4" )
        current_app.logger.debug("output file path: {}".format(output_file_path))
        cmd = ffmpeg_transcode.format(input_file_path, output_file_path)
        current_app.logger.info("ffmpeg command to execute is : {}".format(cmd))
        return cmd

    except Exception as e:
        current_app.logger.error("failled to create fffmpeg command with error: {}".format(e))
        return None


def ffmpeg_run(cmd):
    """
    WIll run a specified command over SSH
    :param cmd: The command to be executed on the remote machine
    :return: A JSON object containing the stdout of the executed command, and the success flag
    """
    try:
        ssh = init_ssh_client()
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd)
        current_app.logger.debug("Executed cmd: %s \n", cmd)
        # if ssh return success code: 0
        if ssh_stdout.channel.recv_exit_status() == 0:
            output = ""
            # ssh command returns an array containing the JSON output,
            # parse JSON
            for line in ssh_stdout.readlines():
                output += line.rstrip()
            ssh.close()
            if is_json(output):
                output = json.loads(output)
                output['success'] = True
                return output
            return {"success": True}
        current_app.logger.error('FFmpeg error\n')
        ssh.close()
    except:
        return {"failed": False}
    return {"success": False}

def is_json(myjson):
    try:
        json.loads(myjson)
    except ValueError:
        return False
    return True

