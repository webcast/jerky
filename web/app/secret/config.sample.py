ADMIN_EGROUP = "webcast-team"
APP_PORT = 59999
SECRET_KEY = "E\xf0\xd2G\xd5\x0bJ\xfd\x0b\xc7\xdc\x8c\xfb\xbd\xf7\xcd&C\xa3\xbc\xc8\xf7\xeb5"

# Debug and logging configuration
DEBUG = True
IS_LOCAL_INSTALLATION = True
TESTING = False

WTF_CSRF_ENABLED = True

LOG_LEVEL = "DEV"
WEBAPP_LOGS = "/opt/app-root/logs/webapp.log"

# If you want to run https(for local dev) use 'adhoc' otherwise None
SSL = 'adhoc'

# Whether or not to use the wsgi Proxy Fix
USE_PROXY = True

# Oauth config
CERN_OAUTH_CLIENT_ID = "macpro01_clientp"
CERN_OAUTH_CLIENT_SECRET = "KNlHxb0WQAmwNEkUrTlkCvDnt2N80VaMjpRlz0UoqAE1"
CERN_OAUTH_AUTHORIZE_URL = "https://oauth.web.cern.ch/OAuth/Authorize"
CERN_OAUTH_TOKEN_URL = "https://oauth.web.cern.ch/OAuth/Token"


SQLALCHEMY_POOL_SIZE = 5
SQLALCHEMY_POOL_TIMEOUT = 10
SQLALCHEMY_POOL_RECYCLE = 120
SQLALCHEMY_MAX_OVERFLOW = 3
