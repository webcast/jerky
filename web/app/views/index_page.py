import os
import shutil
from flask import Blueprint, render_template, request, redirect, url_for, session, current_app
from app.forms import MasterPathForm
from app.common import utils


index_page = Blueprint('index_page', __name__,
                       template_folder='templates')

@index_page.route("/", methods=['GET', 'POST'])
def index():
    current_user = session.get('user', None)
    form = MasterPathForm()
    if request.method == 'POST':
        current_app.logger.debug(form.data['originpath'])
        path = os.path.normpath(form.data['originpath'])
        current_app.logger.debug(path)
        cmd = utils.ffmpeg_cmd(path)
        utils.ffmpeg_run(cmd)
        return render_template('job_submitted.html', user=current_user)

    return render_template('index.html',
                           form=form,
                           user=current_user)


@index_page.route('/logout')
def logout():
    session["user"] = None
    return redirect(url_for('index_page.index'))
